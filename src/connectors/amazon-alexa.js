'use strict';

const filter = new MetadataFilter({ all: removeRemasteredAndSo, track: removeRemasteredAndSo, album: removeRemasteredAndSo });

Connector.playerSelector = '#d-content';
Connector.remainingTimeSelector = '.d-np-time-display.remaining-time';
Connector.currentTimeSelector = '.d-np-time-display.elapsed-time';
Connector.trackArtSelector = '#d-album-art > #d-image img';

console.log('Entering');

Connector.getDuration = () => {
	let elapsed = Util.stringToSeconds($(Connector.currentTimeSelector).text());
	let remaining = -Util.stringToSeconds($(Connector.remainingTimeSelector).text());
	return (remaining + elapsed);
};

Connector.getRemainingTime = () => {
	let remaining = -Util.stringToSeconds($(Connector.remainingTimeSelector).text());
	return remaining;
};

//Connector.albumSelector = '#d-info-text .d-sub-text-2';
Connector.getAlbum = () => {
	const album = $('#d-info-text .d-sub-text-2').text();
	if (album === 'Amazon Music') {
		return Connector.getArtistTrack().artist;
	}
	return album;

};
Connector.getArtistTrack = () => {
	if (isPlayingLiveRadio()) {
		const songTitle = $('.d-queue-info .song-title').text();
		return Util.splitArtistTrack(songTitle);
	}

	const artist = $('#d-info-text .d-sub-text-1').text();
	const track = $('#d-info-text .d-main-text').text();
	return { artist, track };
};

Connector.isPlaying = () => {
	const duration = Connector.getDuration();

	/*
	 * The app doesn't update the remaining and elapsed times straight away
	 * when changing songs. The remaining time rolls over and starts counting
	 * down from an hour. This is a workaround to avoid detecting an incorrect
	 * duration time.
	 */
	if (duration > 3600) {
		return false;
	}

	return $('#d-primary-control .play').length === 0;
};

Connector.applyFilter(filter);

function isPlayingLiveRadio() {
	return $('#d-secondary-control-left .disabled').length === 1 &&
		$('#d-secondary-control-right .disabled').length === 1;
}

function removeRemasteredAndSo(track) {
	console.log('Track:', track);
	const replaced = track.replace(/[\(\[][-_ /a-zA-Z0-9]*remaster[-_ /a-zA-Z0-9]*[\)\]]/ig, '')
				.replace(/\[Explicit\]/g, '')
				.replace(/[\(\[] ?deluxe edition[-_ /a-zA-Z0-9]*[\)\]]/ig, '')
				.replace(/[\(\[] ?Album Version ?[\)\]]/ig, '').trim();
	console.log('Track replaced: ', replaced);
	return replaced;
}
